/* jshint esversion: 6 */

/*
 * Constants
 * TODO: get these values by a different way (they are the same on server side)
 */
const WEBSOCKET_PAGE = "ws";
const MESSAGE_LIST = 0;
const MESSAGE_TEXT = 1;
const MESSAGE_UPDATECLIENT = 2;

/*
 * Global vars
 */
var ws;
var myID = 0;

/*
 * HTML elements used several times
 * see funtion "initHTMLElements"
 */
var information_html = 0;
var chatting_html = 0;
var clientsList_html = 0;
var inputMessage_div = 0;
var historyMessage_div = 0;

/*
 * Helper funcitons
 */

// functions for handling the username
function get_user_fullid(id) {
	'use strict';
	console.debug("get_user_fullid: ", id);
	return "user" + id;
}
function get_user_name(id) {
	'use strict';
	console.debug("get_user_name: ", id);
	return "User " + id;
}
function get_my_user_name() {
	'use strict';
	console.debug("get_my_user_name");
	return get_user_name(myID);
}

// functions for handling the radio button
function add_radio(id) {
	'use strict';
	console.debug("add_radio: ", id);

	if (myID != id) {
		clientsList_html.innerHTML += '<div id="' + get_user_fullid(id) + '"><input type="radio" name="receivers" id="' + id + '"><label for="' + id + '">' + get_user_name(id) + '</label></div>';
	}
}
function rm_radio(radio_div) {
	'use strict';
	console.debug("rm_radio: ", radio_div);

	radio_div.parentNode.removeChild(radio_div);
}
function get_radio_checked(radio_name) {
	'use strict';
	console.debug("get_radio_checked: ", radio_name);
	return document.querySelector('input[name=' + radio_name + ']:checked');
}

// initialize global vars for HTML elements
function initHTMLElements() {
	console.debug("initHTMLElements");
	information_html = document.getElementById("information");
	chatting_html = document.getElementById("chatting");
	clientsList_html = document.getElementById("clients_list");
	inputMessage_div = document.getElementById("message");
	historyMessage_div = document.getElementById("history");
}

// functions for handling the history of messages
function historyAdd(className, content) {
	historyMessage_div.innerHTML += '<div class="' + className + '">' + content + '</div>';
}
function historyAddFrom(id) {
	historyAdd("received_from", get_user_name(id));
}
function historyAddText(text) {
	historyAdd("received_message", text);
}

/*
 * Communication
 */
function write_message() {
	'use strict';
	console.debug("write_message");
	var destination = get_radio_checked("receivers");
	if (destination != null) {
		if (inputMessage_div.value == "") {
			alert("Please, enter a message");
		}
		else {
			var object = {
				Type: MESSAGE_TEXT,
				Id: parseInt(destination.id),
				Text: inputMessage_div.value
			};
			console.debug("object: ", object);
			ws.send(JSON.stringify(object));
			inputMessage_div.value = "";
		}
	}
	else
	{
		alert("Select a user before sending a message");
	}
}
function read_message(message) {
	'use strict';
	console.debug("read_message: '", message, "'");

	// transform the message into a JSON object
	var jsonMsg = JSON.parse(message);

	switch(jsonMsg.Type) {
		case MESSAGE_LIST:
			// store my own ID
			myID = jsonMsg.Id;
			// create a radio button for each client
			jsonMsg.List.forEach(add_radio);
			// change the "information" message and show the "chatting" zone
			information_html.innerText = "connected as \"" + get_my_user_name() + "\"";
			chatting_html.style.display = "block";
			break;
		case MESSAGE_UPDATECLIENT:
			var id = jsonMsg.Id;
			var radio = document.getElementById(get_user_fullid(id));
			if (radio != null) {
				rm_radio(radio);
			}
			else {
				add_radio(id);
			}
			break;
		case MESSAGE_TEXT:
			historyAddFrom(jsonMsg.Id);
			historyAddText(jsonMsg.Text);
			break;
		default:
			// TODO: handle error
	}
}

/*
 * Conection handlers
 */
function connection_closed() {
	'use strict';
	console.debug("connection_closed");
	information_html.innerText = "connection closed, please reload page to reconnect";
	chatting_html.style.display = "hidden";
}
function connect() {
	'use strict';
	console.debug("connect to " + window.location.hostname);

	initHTMLElements();

	if ("WebSocket" in window) {
		ws = new WebSocket("ws://" + window.location.hostname + ":" + window.location.port + "/" + WEBSOCKET_PAGE);
		ws.onmessage = function(evt) {
			var received_msg = evt.data;
			read_message(received_msg);
		};
		// TODO: onclose // error, etc. call "connection_closed"
	}
	else {
		information_html.innerText = "impossible to join the message system, your browser does not support WebSocket";
	}
}
function disconnect() {
	'use strict';
	console.debug("disconnect");
	connection_closed();
	ws.close();
}
