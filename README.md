# Content

This project is a very basic messaging system (kind of targeted chat). The principle is the following:

If client A sends a message M to client B and B is known by the service, the service should deliver M to B.

The aim of this project is to learn Golang... and actually update my knowledge on HTML/CSS/JS as well!

WebSocket and WebRTC are new for me as well :). A very small project for so many new cool stuffs.

# Build

## Prerequisites

On Ubuntu 18.10, run ("docker.io" is _optionnal_):

``` bash
sudo apt install golang git docker.io
```

## Compile

``` bash
make
```

_Optionnal_: build the docker image localy with:

``` bash
make image
```

_Optionnal_: remove binary with:

``` bash
make clean
```

## Run the binary

``` bash
./directchat
```

Then, run web clients by opening http://localhost:8080.

## Stop

push keys "CTRL-C"

# Docker

## Prerequisites

On Ubuntu 18.10, run:

``` bash
sudo apt install docker.io
```

## Pull the image

From the [GitLab](gitlab.com) private registry (see [this webpage for more information on how to login to the registry if needed](https://gitlab.com/pascal-vs/learn-go-chat/container_registry)):

``` bash
docker pull registry.gitlab.com/pascal-vs/learn-go-chat/directchat:latest
```

## Run the container

``` bash
docker run -p 8080:8080 --rm --detach --name directchat registry.gitlab.com/pascal-vs/learn-go-chat/directchat:latest
```

Then, run web clients by opening http://localhost:8080.

## Stop

``` bash
docker stop directchat
```
