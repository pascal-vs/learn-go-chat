package main

const (
	// Allowed types of messages
	MESSAGE_LIST = 0
	MESSAGE_TEXT = 1
	MESSAGE_UPDATECLIENT = 2
)

// Basic Message: type + ID
type MsgBase struct {
	Type int
	Id uint32
}
type MsgBaseAPI interface {
	initMessage(uint32)
}

// Extend the Basic Message: add a list of IDs
type MsgList struct {
	MsgBase
	List []uint32
}
func (msg *MsgList) initMessage(id uint32) {
	msg.Type = MESSAGE_LIST
	msg.Id = id
}
func (msg *MsgList) setList(list map[uint32]*Client) {
	for id := range list {
		msg.List = append(msg.List, id)
	}
}

// Extend the Basic Message: add a text
type MsgText struct {
	MsgBase
	Text string
}
func (msg *MsgText) initMessage(id uint32) {
	msg.Type = MESSAGE_TEXT
	msg.Id = id
}
func (msg *MsgText) setText(s string) {
	msg.Text = s
}

// Usable form of the Basic Message (only one ID is useful for this type)
type MsgUpdateClient struct {
	MsgBase
}
func (msg *MsgUpdateClient) initMessage(id uint32) {
	msg.Type = MESSAGE_UPDATECLIENT
	msg.Id = id
}
