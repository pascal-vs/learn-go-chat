package main

import (
	"encoding/json"
	"errors"
	"log"
)

/*
 * Hub of clients
 * Save the clients inside a map to avoid space waste due to disconnections
 * Use channels for register/unregister to make the map usage thread safe
 */
type Hub struct {
	// Registered clients
	clients map[uint32]*Client
	// Maximum ID given to a client (must start at 1!)
	maxId uint32
	// Register requests from the clients
	register chan *Client
	// Unregister requests from clients
	unregister chan uint32
}

func newHub() *Hub {
	return &Hub{
		clients:    make(map[uint32]*Client),
		maxId:      0,
		register:   make(chan *Client),
		unregister: make(chan uint32),
	}
}

func (h *Hub) getNextId() (uint32, error) {
	h.maxId++
	if h.maxId == 0 {
		return 0, errors.New("[hub] impossible to get a new ID, limit reached")
	}
	return h.maxId, nil
}

func (h *Hub) updateNeighborhood(updatedId uint32) {
	// messageUpdateClient will be sent to other clients
	var messageUpdateClient MsgUpdateClient
	messageUpdateClient.initMessage(updatedId)

	// transform the structure into a "JSON style" []byte
	b, err := json.Marshal(messageUpdateClient)
	if err != nil {
		log.Printf("error: %v", err)
		return
	}
	for id, c := range h.clients {
		if id != updatedId {
			// send it!
			c.send <- b
		}
	}
}

func (h *Hub) initNeighborhood(newId uint32) {
	// messageList will be sent to the new client
	var messageList MsgList
	messageList.initMessage(newId)

	// put all IDs in the slice
	messageList.setList(h.clients)
	// transform the structure into a "JSON style" []byte
	b, err := json.Marshal(messageList)
	if err != nil {
		log.Printf("error: %v", err)
		return
	}
	// send it!
	h.clients[newId].send <- b
}

func (h *Hub) registerClient(id uint32, c *Client) {
	h.clients[id] = c
}

func (h *Hub) unregisterClient(id uint32, c *Client) {
	close(c.send)
	delete(h.clients, id)
}

func (h *Hub) run() {
	for {
		select {
		case client := <- h.register:
			// get the next available ID
			clientId, err := h.getNextId()
			if err != nil {
				log.Printf("error: %v", err)
				close(client.send)
			}
			// give the ID to the client
			client.attachId(clientId)
			// register this client as an active one
			log.Printf("[hub] registering client %v\n", clientId)
			h.registerClient(clientId, client)
			// send the list of all client IDs to the new client
			h.initNeighborhood(clientId)
			// and send this new client to all the other ones
			h.updateNeighborhood(clientId)
		case clientId := <- h.unregister:
			log.Printf("[hub] unregistering client %v\n", clientId)
			if client, ok := h.clients[clientId] ; ok {
				// unregister this client from the list of active ones
				h.unregisterClient(clientId, client)
				// and inform other clients
				h.updateNeighborhood(clientId)
			} else {
				log.Printf("[hub] client %v not found\n", clientId)
			}
		}
	}
}
