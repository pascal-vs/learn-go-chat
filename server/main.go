package main

import (
	"flag"
	"log"
	"net/http"
	"os"
)

const (
	logFileName = "server.log"
)

// TODO: add a flag for unit tests
var addr = flag.String("addr", ":8080", "http service address")

// serve the client web interface when loading "/"
func serveHome(w http.ResponseWriter, r *http.Request) {
	log.Println(r.URL)
	if r.URL.Path != "/" {
		http.Error(w, "Not found", http.StatusNotFound)
		return
	}
	if r.Method != "GET" {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}
	http.ServeFile(w, r, "web/index.html")
}

func main() {
	// read command line flags
	flag.Parse()

	// open/create the log file
	f, err := os.OpenFile(logFileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		log.Fatalf("error opening file: %v", err)
	}
	defer f.Close()

	// set the default logger to the log file
	log.SetOutput(f)

	// run the clients hub in a dedicated goroutine
	hub := newHub()
	go hub.run()

	// serve the web client interface when asking for home page
	http.HandleFunc("/", serveHome)
	// serve static files for the web interface (css, js)
	http.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir("web/static"))))
	// handle the connection to a WebSocket by the client
	http.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
		serveWs(hub, w, r)
	})
	// run the web server here
	err = http.ListenAndServe(*addr, nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
