package main

import (
	"encoding/json"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/websocket"
)

const (
	// Time allowed to write a message to the peer
	writeWait = 10 * time.Second
	// Time allowed to read the next pong message from the peer
	pongWait = 60 * time.Second
	// Send pings to peer with this period. Must be less than pongWait
	pingPeriod = (pongWait * 9) / 10
	// Maximum message size allowed from peer
	// in HTML, the text input element is limited to 512, the extra 512 here should be enough :)
	maxMessageSize = 1024
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  maxMessageSize,
	WriteBufferSize: maxMessageSize,
}

// Client is a middleman between the websocket connection and the hub
type Client struct {
	// Pointer to the hub:
	// - avoid the use of a global singleton
	// - allow several hubs if required in the future
	hub *Hub
	// ID of this client (useful for unregistration)
	id uint32
	// The websocket connection
	conn *websocket.Conn
	// Buffered channel of outbound messages
	send chan []byte
}

// Read 'JSON' messages from the websocket
func (c *Client) readJson() {
	defer func() {
		c.hub.unregister <- c.id
		c.conn.Close()
	}()

	c.conn.SetReadLimit(maxMessageSize)
	c.conn.SetReadDeadline(time.Now().Add(pongWait))
	c.conn.SetPongHandler(func(string) error { c.conn.SetReadDeadline(time.Now().Add(pongWait)); return nil })
	for {
		_, message, err := c.conn.ReadMessage()
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				log.Printf("error: %v", err)
			}
			break
		}
		log.Println("[read] message: ", message)

		// unmarshal the object received into a JSON object
		var text MsgText
		if err := json.Unmarshal(message, &text); err != nil {
			log.Printf("error: %v", err)
			return
		}
		// save the embedded ID
		destnationId := text.Id
		// replace the embedded ID by my ID => the message is tranformed from a "TO" message into a "FROM" message
		text.Id = c.id

		// marshal the object
		b, err := json.Marshal(text)
		if err != nil {
			log.Printf("error: %v", err)
			return
		}
		// and send the changed message to "destinationId"
		c.hub.clients[destnationId].send <- b
	}
}

// Write 'JSON' messages to the websocket
func (c *Client) writeJson() {
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		ticker.Stop()
		c.conn.Close()
	}()
	for {
		select {
		case message, ok := <-c.send:
			log.Println("[send] message: ", message)
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if !ok {
				// the hub closed the channel
				c.conn.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}

			w, err := c.conn.NextWriter(websocket.TextMessage)
			if err != nil {
				return
			}
			w.Write(message)

			if err := w.Close(); err != nil {
				return
			}
		case <-ticker.C:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if err := c.conn.WriteMessage(websocket.PingMessage, nil); err != nil {
				return
			}
		}
	}
}

// Give an ID to this client
func (c *Client) attachId(id uint32) {
	c.id = id
}

// Manage WebSocket requests from the clients
func serveWs(hub *Hub, w http.ResponseWriter, r *http.Request) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}
	log.Printf("[client] new connection!\n")
	client := &Client{hub: hub, conn: conn, send: make(chan []byte, maxMessageSize)}
	client.hub.register <- client

	// 2 go routines per client
	// TODO: study impact on performance / scalability
	// 1 go routine for writing data and 1 go routine for reading data
	go client.writeJson()
	go client.readJson()
}
