SHELL = /bin/sh

### --- Config
# compilation
EXEC = directchat
SRC = $(wildcard server/*.go)
# docker
IMG = registry.gitlab.com/pascal-vs/learn-go-chat/directchat:latest
WEBPAGES = web
DOCKERCTXT = docker

.PHONY: all image clean

all: $(EXEC)

### --- Compiler options
$(EXEC):	GOFLAGS := -a -tags netgo -ldflags="-w -s"
$(EXEC):	GOCTXT := CGO_ENABLED=0 GOOS=linux GOARCH=amd64

### --- Executable
$(EXEC): $(SRC)
	@go get -u -v github.com/gorilla/websocket
	@$(GOCTXT) go build $(GOFLAGS) -o $(EXEC) $(SRC)

### --- Docker Image
image: $(EXEC)
	@cp $(EXEC) $(DOCKERCTXT)/
	@cp -r $(WEBPAGES) $(DOCKERCTXT)/
	@docker build $(DOCKERCTXT)/ --tag $(IMG)
	@rm -f $(DOCKERCTXT)/$(EXEC)
	@rm -rf $(DOCKERCTXT)/$(WEBPAGES)

### --- Miscellaneous
clean:
	@rm -f $(EXEC) > /dev/null
